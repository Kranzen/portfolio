$(function () {
  /* Change active state */
  $('.nav li').on('click', function () {
    $('.nav li').removeClass('active');
    $(this).addClass('active');
    var slideToo = $(this).attr('href');
    console.log(slideToo);
    $('html, body').animate({ scrollTop: $(slideToo).offset().top }, 1000);
  });
});
